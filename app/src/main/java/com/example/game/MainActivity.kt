package com.example.game

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    private val REQUEST_MAIN = 11
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnPlus = findViewById<Button>(R.id.btnPlus)
        btnPlus.setOnClickListener {
            val intent = Intent(MainActivity@this, pageplusActivity::class.java)
            startActivity(intent)
        }

        val btnMinus = findViewById<Button>(R.id.btnMinus)
        btnMinus.setOnClickListener {
            val intent = Intent(MainActivity@this, pageminusActivity::class.java)
            startActivity(intent)
        }

        val btnMultiply = findViewById<Button>(R.id.btnMultiply)
        btnMultiply.setOnClickListener {
            val intent = Intent(MainActivity@this, pageMultiplyActivity::class.java)
            startActivity(intent)
        }
    }
}