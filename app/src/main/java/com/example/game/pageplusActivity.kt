package com.example.game

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_pageplus.*
import kotlin.random.Random

class pageplusActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pageplus)
        fun Play(){
            val random1: Int = Random.nextInt(10) + 1
            txt1.setText(Integer.toString(random1))

            val random2: Int = Random.nextInt(10) + 1
            txt2.setText(Integer.toString(random2))

            val sum = random1 + random2

            val position: Int = Random.nextInt(3) + 1

            if(position == 1){
                btn1.setText(Integer.toString(sum))
                btn2.setText(Integer.toString(sum + 1))
                btn3.setText(Integer.toString(sum - 1))
            }else if(position == 2){
                btn1.setText(Integer.toString(sum))
                btn2.setText(Integer.toString(sum + 2))
                btn3.setText(Integer.toString(sum - 2))
            }else{
                btn1.setText(Integer.toString(sum))
                btn2.setText(Integer.toString(sum + 3))
                btn3.setText(Integer.toString(sum - 3))
            }

            btn1.setOnClickListener {
                if(btn1.text.toString().toInt() == sum){
                    txtAnswer.setText("Correct")
                    txtCorrect.text = (txtCorrect.text.toString().toInt()+ 1).toString()
                }else{
                    txtAnswer.setText("Wrong")
                    txtWrong.text = (txtWrong.text.toString().toInt()+ 1).toString()
                }
            }

            btn2.setOnClickListener {
                if(btn2.text.toString().toInt() == sum){
                    txtAnswer.setText("Correct")
                    txtCorrect.text = (txtCorrect.text.toString().toInt()+ 1).toString()
                }else{
                    txtAnswer.setText("Wrong")
                    txtWrong.text = (txtWrong.text.toString().toInt()+ 1).toString()
                }
            }

            btn3.setOnClickListener {
                if(btn3.text.toString().toInt() == sum){
                    txtAnswer.setText("Correct")
                    txtCorrect.text = (txtCorrect.text.toString().toInt()+ 1).toString()
                }else{
                    txtAnswer.setText("Wrong")
                    txtWrong.text = (txtWrong.text.toString().toInt()+ 1).toString()
                }
            }
        }
        Play()
        btnNext.setOnClickListener {
            txtAnswer.setText("Please Select an Answer")
            Play()
        }

        val btnHome = findViewById<Button>(R.id.btnHome)
        btnHome.setOnClickListener {
            val intent = Intent(MainActivity@this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}